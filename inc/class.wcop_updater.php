<?php

class wcop_updater {

    private $slug;

    private $pluginData;

    private $username;

    private $repo;

    private $pluginFile;

    private $bitbucketAPIResult;

    private $pluginActivated;

    /**
     * Class constructor.
     *
     * @param  string $pluginFile
     * @param  string $bitBucketUsername
     * @param  string $bitBucketProjectName
     * @param  string $accessToken
     * @return null
     */
    function __construct( $pluginFile, $bitBucketUsername, $bitBucketProjectName)
    {
        add_filter( "pre_set_site_transient_update_plugins", array( $this, "setTransitent" ) );
        add_filter( "plugins_api", array( $this, "setPluginInfo" ), 10, 3 );
        add_filter( "upgrader_pre_install", array( $this, "preInstall" ), 10, 3 );
        add_filter( "upgrader_post_install", array( $this, "postInstall" ), 10, 3 );

        $this->pluginFile 	= $pluginFile;
        $this->username 	= $bitBucketUsername;
        $this->repo 		= $bitBucketProjectName;
    }

    /**
     * Get information regarding our plugin from WordPress
     *
     * @return null
     */
    private function initPluginData()
    {
		$this->slug = plugin_basename( $this->pluginFile );

		$this->pluginData = get_plugin_data( $this->pluginFile );
    }

    /**
     * Get information regarding our plugin from Bitbucket
     *
     * @return null
     */
    private function getRepoReleaseInfo()
    {
        if ( ! empty( $this->bitbucketAPIResult ) )
        {
    		return;
		}

		// Query the Bitbucket API
		$url = "https://api.bitbucket.org/2.0/repositories/{$this->username}/{$this->repo}/refs/tags";

		if ( ! empty( $this->accessToken ) )
		{
		    $url = add_query_arg( array( "access_token" => $this->accessToken ), $url );
		}
		// Get the results
		$this->bitbucketAPIResult = wp_remote_retrieve_body( wp_remote_get( $url ) );

		if ( ! empty( $this->bitbucketAPIResult ) )
		{
		    $this->bitbucketAPIResult = @json_decode( $this->bitbucketAPIResult );
		}
		// Use only the latest release
		if ( is_object( $this->bitbucketAPIResult ) )
		{
		    $this->bitbucketAPIResult = $this->bitbucketAPIResult->values[count($this->bitbucketAPIResult->values)-1];
			$readme='https://bitbucket.org/it-mooti/wcop/raw/'.$this->bitbucketAPIResult->target->hash."/readme.txt";
			$this->bitbucketAPIResult->body = wp_remote_retrieve_body( wp_remote_get( $readme ) );
		}
    }

    /**
     * Push in plugin version information to get the update notification
     *
     * @param  object $transient
     * @return object
     */
    public function setTransitent( $transient )
    {
        if ( empty( $transient->checked ) )
        {
    		return $transient;
		}

		// Get plugin & Bitbucket release information
		$this->initPluginData();
		$this->getRepoReleaseInfo();
		$doUpdate = version_compare( $this->bitbucketAPIResult->name, $transient->checked[$this->slug] );
		if ( $doUpdate )
		{
			$package = "https://bitbucket.org/{$this->username}/{$this->repo}/get/".$this->bitbucketAPIResult->name.".zip";
			
			// Plugin object
			$obj = new stdClass();
			$obj->slug = $this->slug;
			$obj->name = $this->slug;
			$obj->new_version = $this->bitbucketAPIResult->name;
			$obj->url = $this->pluginData["PluginURI"];
			$obj->package = $package;

			$transient->response[$this->slug] = $obj;
		}

        return $transient;
    }

    /**
     * Push in plugin version information to display in the details lightbox
     *
     * @param  boolean $false
     * @param  string $action
     * @param  object $response
     * @return object
     */
    public function setPluginInfo( $false, $action, $response )
    {
		$this->initPluginData();
		$this->getRepoReleaseInfo();

		if ( empty( $response->slug ) || $response->slug != $this->slug )
		{
		    return $false;
		}

		// Add our plugin information
		$response->last_updated = $this->bitbucketAPIResult->target->date;
		$response->slug = $this->slug;
		$response->plugin_name  = $this->pluginData["Name"];
		$response->version = $this->bitbucketAPIResult->name;
		$response->author = $this->pluginData["AuthorName"];
		$response->homepage = $this->pluginData["PluginURI"];

		// This is our release download zip file
		$downloadLink = "https://bitbucket.org/{$this->username}/{$this->repo}/get/".$this->bitbucketAPIResult->name.".zip";

		$response->download_link = $downloadLink;

		// Gets the Changelog
		$changelog=explode( "== Changelog ==", $this->bitbucketAPIResult->body);
		if ( isset( $changelog[1] ) ) {
		    $changelog = nl2br($changelog[1]);
		}
		else{
			$changelog="";
		}
		$description=explode( "== Description ==", $this->bitbucketAPIResult->body);
		if ( isset( $description[1] ) ) {
		    $description = explode( "==", $description[1]);
			$description=nl2br($description[0]);
		}
		else{
			$description="";
		}
		
		$response->sections = array(
			'Description' 	=> $description,
			'changelog' 	=> $changelog
		);
		
		// Gets the required version of WP if available
		$matches = null;
		preg_match( "/Requires at least:\s([\d\.]+)/i", $this->bitbucketAPIResult->body, $matches );
		if ( ! empty( $matches ) ) {
		    if ( is_array( $matches ) ) {
		        if ( count( $matches ) > 1 ) {
		            $response->requires = $matches[1];
		        }
		    }
		}

		// Gets the tested version of WP if available
		$matches = null;
		preg_match( "/Tested up to:\s([\d\.]+)/i", $this->bitbucketAPIResult->body, $matches );
		if ( ! empty( $matches ) ) {
		    if ( is_array( $matches ) ) {
		        if ( count( $matches ) > 1 ) {
		            $response->tested = $matches[1];
		        }
		    }
		}

        return $response;
    }

    /**
     * Perform check before installation starts.
     *
     * @param  boolean $true
     * @param  array   $args
     * @return null
     */
    public function preInstall( $true, $args )
    {
        // Get plugin information
		$this->initPluginData();

		// Check if the plugin was installed before...
    	$this->pluginActivated = is_plugin_active( $this->slug );
    }

    /**
     * Perform additional actions to successfully install our plugin
     *
     * @param  boolean $true
     * @param  string $hook_extra
     * @param  object $result
     * @return object
     */
    public function postInstall( $true, $hook_extra, $result )
    {
		global $wp_filesystem;

		// Since we are hosted in Bitbucket, our plugin folder would have a dirname of
		// reponame-commit change it to our original one:
		$pluginFolder = WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . dirname( $this->slug );
		$wp_filesystem->move( $result['destination'], $pluginFolder );
		$result['destination'] = $pluginFolder;

		// Re-activate plugin if needed
		if ( $this->pluginActivated )
		{
		    $activate = activate_plugin( $this->slug );
		}

        return $result;
    }
}