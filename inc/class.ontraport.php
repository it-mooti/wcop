<?php
use OntraportAPI\Ontraport;
class wcopOntraport{
	public $client;
	public $objectTypes=array(
		"0"=>"contact",
		"14"=>"tag",
		"52"=>"order",
		"16"=>"product",
		"17" => "purchase",
		"65" =>"Offer",
		"5" => "sequence",
		"9" => "step",
		"43" => "WordPressMemberships",
		"12" => "Notes",
		"45" => "CreditCards",
		"46" => "transaction",
		"44" => 'openorders'
	);
	public function __construct($appId, $apiKey){
		$this->client = new Ontraport( $appId, $apiKey); 
	}
	public function is_valid(){
		$response = json_decode( $this->client->contact()->retrieveMeta() );
		if(!isset($response->data)){
			return false;
		}
		return true;
	}
	public function build_query( $condition ){
		$query=array();
		$i=1;
		foreach( $condition as $k => $v ){
			$query[] = array(
				"field" => array(
					"field" => $k
				),
				"op" => "=",
				"value" => array(
					"value" => $v
				)
			);
			if( $i != count( $condition ) ) {
				$query[] = "AND";
			}
			$i++;
		}
		return json_encode( $query );
	}
	
	public function searchContact( $condition='', $keyword='' ){
		return $this->searchObject( "contact", $condition, $keyword );
	}
	
	public function searchObject( $objectType, $condition='', $keyword='', $args = array() ){
		if(is_array($condition)){
			$condition = $this->build_query( $condition );
		}
		$requestParams = array(
			"objectID" => $this->getObjectID($objectType),
		);
		if( !empty( $condition ) ) {
			$requestParams[ "condition" ] = $condition;
		}
		if( !empty( $keyword ) ) {
			$requestParams[ "keyword" ] = $keyword;
		}
		$requestParams = array_merge( $requestParams, $args );
		if( isset( $requestParams[ "range" ] ) ) {
			$limit = $requestParams[ "range" ];
		}
		else {
			/*$response = json_decode( $this->client->object()->retrieveCollectionInfo($requestParams) );
			if( isset( $response->data->count ) ) {
				$limit = $response->data->count;
			}
			else {
				$limit = 50;
			}*/
			$limit = 0;
		}
		if( $limit > 0 && $limit <= 50 ) {
			$requestParams[ "range" ] = $limit;
		}
		else {
			$requestParams[ "range" ] = 50;
		}
		$start = 0;
		$result = array();
		do{
			$requestParams[ "start" ] = $start;
			$response = json_decode( $this->client->object()->retrieveMultiple($requestParams) );
			if( is_array( $response->data ) ) {
				$result = array_merge( $result, $response->data );
			}
			$start += 50;
		} while( $limit > 0 && $start < $limit || is_array( $response->data ) && count( $response->data ) == 50 );
		return $result;
	}
	
	public function findContactByEmail( $email ){
		$contact=$this->searchContact( array( "email" => $email ) );
		if(is_array($contact)){
			$contact=$contact[0];
		}
		else{
			$contact=false;
		}
		return $contact;
	}
	
	public function getContactByID( $id ){
		$requestParams = array(
			"id" => $id
		);
		$contact = json_decode( $this->client->contact()->retrieveSingle($requestParams) );
		return $contact->data;
	}
	
	public function getObject( $objectType, $id ){
		$requestParams = array(
			"objectID" => $this->getObjectID($objectType),
			"id" => $id
		);
		$response = json_decode( $this->client->object()->retrieveSingle($requestParams) );
		return $response->data;
	}
	
	public function saveUserData($contactID, $fields){
		return $this->saveObject( "contact", $contactID, $fields);
	}
	
	public function saveObject($objectType, $objectID, $fields){
		
		$requestParams=array_merge( array(
			"objectID" => $this->getObjectID($objectType)
		), $fields);
		
		if( $objectID>0 ){
			$requestParams[ "id" ] = $objectID;
			$response = $this->client->object()->update($requestParams);
		}
		else {
			$response = $this->client->object()->create($requestParams);
		}
		$response = json_decode( $response );
		if( isset( $response->data ) ) {
			return $response->data;
		}
		else {
			return false;
		}
	}
	
	public function getObjectID($objectIDorName){
		return $objectIDorName>0?$objectIDorName:array_search( $objectIDorName, $this->objectTypes );
	}
	
	public function getTagName($tagID){
		
		$tagName="";
		$requestParams = array(
			"objectID" => $this->getObjectID("tag"), // Tag object
			"id"       => $tagID
		);
		$tag = json_decode( $this->client->object()->retrieveSingle( $requestParams ) );
		if( isset( $tag->data->tag_name ) ){
			$tagName = $tag->data->tag_name;
		}
		return $tagName;
	}
	
	public function getTagsArray($contactCat){
		
		$tags=array();
		if( !empty( $contactCat ) ){
			$tagIDs = explode( '*/*', trim( $contactCat, '*/*' ) );
			foreach( $tagIDs as $tagID ){
				$tag = $this->getTagName( $tagID );
				$tags[ $tagID ]= $tag;
			}
		}
		return $tags;
	}
	
	public function getContactFields( $refresh=0 ) {
        return $this->getObjectFields( 'contact', $refresh );
	}
	
	public function getObjectFields( $objectType, $refresh = 0 ) {
		
		$objectID = $this->getObjectID( $objectType );
		
		if ( !isset( $this->objectFields[ $objectID ] ) || is_null($this->objectFields[ $objectID ]) || $refresh==1 ) {
			$requestParams = array(
				"objectID" => $objectID,
				"format"   => "byId"
			);
			$response = json_decode( $this->client->object()->retrieveMeta( $requestParams ) );
			if( isset( $response->data->$objectID->fields ) ){
				$this->objectFields[ $objectID ]=$response->data->$objectID->fields;
			}
		}
		return $this->objectFields[ $objectID ];
	}
	
	public function getFieldAlias($fieldKey){
		$contact_field = $this->getContactFields();
		if( !empty( $fieldKey ) && isset( $contact_field->$fieldKey ) ){
			return $contact_field->$fieldKey->alias;
		}
		return "";
	}
	
	public function getDropOption( $field_id, $id ){
		return $this->getObjectDropOption( "contact", $field_id, $id );
	}
	
	public function getObjectDropOption( $objectType, $field_id, $id ) {
		$fields = $this->getObjectFields( $objectType );
		if( isset( $fields->$field_id->options->$id ) ){
			return $fields->$field_id->options->$id;
		}
	}
	
	public function getTags() {
		$tags=array();
		try {
			$requestParams = array(
				"objectID"   => $this->getObjectID('tag'),
			);
			$start=0;
			do{
				$requestParams[ "start" ] = $start;
				$tags_loop = json_decode( $client->object()->retrieveMultiple( $requestParams ) );
				if( isset( $tags_loop->data ) && is_array( $tags_loop->data )){
					$tags = array_merge( $tags, $tags_loop->data );
					$start += 50;
				}
			} while( isset( $tags_loop->data ) && is_array( $tags_loop->data ) && count( $tags_loop ) == 50 );
		} catch( Exception $e ) {
			error_log( "Error in Fetching contact records" );
		}
		return $tags;
	}
	
	public function manual_transaction( $transaction_object ) {
		$payment = $this->client->transaction()->processManual( $transaction_object );
		$result = json_decode( $payment );
		if( isset( $result->data ) ) {
			return $result->data;
		}
		else {
			return false;
		}
	}
	
	public function saveorupdate( $objectType, $objectID, $fields ){
		 $requestParams=array_merge( array(
			"objectID" => $this->getObjectID($objectType)
		), $fields);
		
		if( $objectID>0 ){
			$requestParams[ "id" ] = $objectID;
		}
		$response = $this->client->object()->saveOrUpdate($requestParams);
		if( isset( $response->data ) ) {
			return $response->data;
		}
		else {
			return false;
		}
	}
	
	public function addTag( $tag_name, $id, $objectType ){
		$requestParams = array(
			"objectID"  => $this->getObjectID($objectType), // Object type ID: 0
			"ids"       => array($id),
			"add_names" => array(
				$tag_name
			)
		);
		$this->client->object()->addTagByName($requestParams);
		return true;
	}
	
	public function deleteObject( $parameters ){
		$this->client->object()->deleteSingle( $parameters );
	}
	
	public function Request( $object, $parameters = array(), $post=false, $request_type="get"){
		if($post && $request_type=="get"){
			$request_type="post";
		}
		if(in_array($object, array("cdata", "fdata", "pdata"))){
			$parameters = array_merge(array("appid"=>$this->appId, "key"=>$this->apiKey), $parameters);
			$post_parameters=http_build_query($parameters, '', '&');
			$request = "https://api.ontraport.com/".$object.".php";
			$args=array(
				'method'=>'POST',
				'timeout'=>60,
				'body' => $post_parameters,
				'sslverify'=>false,
			);
			$response=wp_remote_request($request, $args);
			if ( is_wp_error( $response ) ) {
			   $error_message = $response->get_error_message();
			   wct_add_notice("Something went wrong: $error_message", "error");
			} else {
				libxml_use_internal_errors(true);
				$result = simplexml_load_string($response["body"]);
			   	if($result){
					return $result;
				}
			}
			return false;
		}
		//New API
		$result = $this->client->request( $parameters, $object , $request_type, array(), NULL );
		if( strpos( $result, '{' ) !== false ){
			$result = json_decode( $result );
			if(!isset($result->code) || $result->code!="0"){
				return false;
			}
			
			if(isset($result->data)){
				return $result->data;
			}
		}
		else{
			return $result;
		}
	}
}