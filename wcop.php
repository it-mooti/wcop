<?php
/*
Plugin Name: WooCommerce to Ontraport Sync
Plugin URI: https://www.itmooti.com/
Description: Custom Plugin
Version: 1.2
Author: ITMOOTI
Author URI: https://www.itmooti.com/
*/
require_once( 'vendor/autoload.php' );
require_once( 'inc/class.ontraport.php' );
require_once( 'inc/class.wcop_updater.php' );

class wcop {
    private $options;
    private $ontraport;
    public function __construct(){
        if ( is_admin() ) {
            new wcop_updater( __FILE__, 'it-mooti', "wcop" );
        }
        add_action('init', array($this, 'init'));
    }
    function get_options(){
        $this->options = array(
            "app_id" => get_option( "wcop_app_id" ),
            "api_key" => get_option( "wcop_api_key" ),
            "program" => get_option( "wcop_program" ),
            "product_name_prefix" => get_option( "wcop_product_name_prefix" ),
            "op_tag" => get_option( "wcop_op_tag" ),
            "sync_order" => get_option( "wcop_sync_order" ),
            "sync_order_date" => get_option( "wcop_sync_order_date" ),
            "sync_order_statuses" => get_option( "wcop_sync_order_statuses" ),
            "sync_tax" => get_option( "wcop_sync_tax" ),
            "op_tax" => get_option( "wcop_op_tax" )
        );
    }
    function is_configured(){
        if( !empty( $this->options[ 'app_id' ] ) && !empty( $this->options[ 'api_key' ] ) ) {
            $this->ontraport = new wcopOntraport( $this->options['app_id'], $this->options['api_key'] );
            return true;
        }
        return false;
    }
    public function get_settings(){
        add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
        add_action( 'admin_footer', array( $this, 'footer' ) );
        if( count( $_POST ) > 0 ) {
            delete_option( "wcop_sync_running" );
        }
        $this->get_options();
        $settings = array(
            array( 'type' => 'title', 'title' => __( 'Woocommerce Order to Ontraport Sync', 'woocommerce' ), 'desc' => 'This plugin can automatically sync your Woocommerce Order History into Ontraport Transaction history. There are settings below that need to be configured in order for the sync to work. Add your Ontraport API Credentials to begin:', 'id' => 'wcop-main' ),
            array( 'type' => 'sectionend', 'id' => 'wcop-main' ),
            array( 'type' => 'title', 'title' => __( 'Ontraport API Settings', 'woocommerce' ), 'desc' => 'Set your API and domain below to connect your Ontraport account with Woocommerce.', 'id' => 'wcop-op-details' ),

            array(
                'title'    => __( 'Ontraport APP ID', 'woocommerce' ),
                'desc'     => __( '', 'woocommerce' ),
                'id'       => 'wcop_app_id',
                'type'     => 'text',
                'default'  => isset( $this->options['app_id'] ) ? esc_attr( $this->options['app_id']) : ''
            ),
            array(
                'title'    => __( 'Ontraport API Key', 'woocommerce' ),
                'desc'     => __( '', 'woocommerce' ),
                'id'       => 'wcop_api_key',
                'type'     => 'text',
                'default'  => isset( $this->options['api_key'] ) ? esc_attr( $this->options['api_key']) : ''
            ),
            array(
                'title'    => __( 'Ontraport Subdomain', 'woocommerce' ),
                'desc'     => __( 'This is the subdomain chosen in your Ontraport account and the domain of your promo tool (eg. “moonray”.ontraport.com) (<a href="https://app.ontraport.com/#!/ontraport_admin/trackingpixel" target="_blank">Click here to view settings</a>)', 'woocommerce' ),
                'id'       => 'wcop_program',
                'type'     => 'text',
                'default'  => isset( $this->options['program'] ) ? esc_attr( $this->options['program']) : ''
            ),

            array( 'type' => 'sectionend', 'id' => 'wcop-op-details' ),
        );
        if( $this->is_configured() ) {
            $valid = $this->ontraport->is_valid();
            if( $valid ) {
                $settings[] = array( 'type' => 'title', 'title' => __( 'Product and Tag Settings', 'woocommerce' ), 'desc' => 'Woocommerce Products can be linked to Ontraport Products by adding the Ontraport Product ID to the Woocommerce Product in the custom field called ‘op16_Product_id’. If the product is a Variant Product that you want to split into different Ontraport Products there is a field called op16_Product_id in the expanded variant settings where the Product ID can be added and will override the id listed in the parent product. IF no product_id from Ontraport is set, then a new Product will be created automatically in Ontraport using the name of the Woocommerce Product with a prefix ‘'.(isset( $this->options['product_name_prefix'] ) ? esc_attr( $this->options['product_name_prefix']) : '').'‘ to identify it in Ontraport.', 'id' => 'wcop-products' );
                $settings[] = array(
                    'title'    => __( 'Product Name Prefix', 'woocommerce' ),
                    'desc'     => __( 'e.g. WC Product - ', 'woocommerce' ),
                    'id'       => 'wcop_product_name_prefix',
                    'type'     => 'text',
                    'default'  => isset( $this->options['product_name_prefix'] ) ? esc_attr( $this->options['product_name_prefix']) : ''
                );
                $settings[] = array(
                    'title'    => __( 'Tag', 'woocommerce' ),
                    'desc'     => __( 'Enter tag which will be added to contact. e.g. WooCommerce Order', 'woocommerce' ),
                    'id'       => 'wcop_op_tag',
                    'type'     => 'text',
                    'default'  => isset( $this->options['op_tag'] ) ? esc_attr( $this->options['op_tag']) : ''
                );
                $settings[] = array( 'type' => 'sectionend', 'id' => 'wcop-products' );
                $settings[] = array( 'type' => 'title', 'title' => __( 'Tax Settings', 'woocommerce' ), 'desc' => 'Select how taxes are posted to Ontraport and link your WooCommerce taxes to Ontraport tax records.', 'id' => 'wcop-op-taxes' );
                $wcop_sync_tax = isset( $this->options['sync_tax'] ) ? esc_attr( $this->options['sync_tax']) : '0';
                $settings[] = array(
                    'title'    => __( 'Select Tax sync option', 'woocommerce' ),
                    'desc'     => '',
                    'id'       => 'wcop_sync_tax',
                    'type'     => 'select',
                    'default'  => $wcop_sync_tax,
                    'options'  => array(
                        '0' => 'Tax inclusive, do not itemize taxes from sale in Ontraport.',
                        '1' => 'Tax exclusive,  match taxes from Woocommerce to taxes in Ontraport for all transactions.'
                    )
                );
                if( $wcop_sync_tax == '1' ) {
                    $op_taxes = $this->ontraport->searchObject('63');
                    $options = array();
                    $options[''] = '--Select None--';
                    foreach( $op_taxes as $op_tax ){
                        $options[ $op_tax->id ] = $op_tax->name;
                    }
                    global $wpdb;
                    $found_rates = $wpdb->get_results(
                        "
						SELECT * FROM {$wpdb->prefix}woocommerce_tax_rates
						WHERE 1=1
						ORDER BY tax_rate_order"
                    );
                    if( is_array( $found_rates ) ) {
                        foreach( $found_rates as $tax ) {
                            $settings[] = array(
                                'title'    => $tax->tax_rate_name." (".(empty($tax->tax_rate_country)?"ALL":$tax->tax_rate_country).")",
                                'desc'     => '',
                                'id'       => 'wcop_op_tax['.$tax->tax_rate_id.']',
                                'type'     => 'select',
                                'default'  => isset( $this->options['wcop_op_tax'][$tax->tax_rate_id] ) ? esc_attr( $this->options['wcop_op_tax'][$tax->tax_rate_id]) : '',
                                'class'    => 'wcop_tax_fields',
                                'options'  => $options
                            );
                        }
                    }
                }
                $settings[] = array( 'type' => 'sectionend', 'id' => 'wcop-op-taxes' );
                $settings[] = array( 'type' => 'title', 'title' => __( 'Test an Order', 'woocommerce' ), 'desc' => 'Before turning on the Sync Engine you can test 1 or more Orders. Enter a WooCommerce Order ID below and click ‘Sync’ to send that order to Ontraport and then make sure that it looks correct before turning on the Sync.', 'id' => 'wcop-op-test-order' );
                $settings[] = array(
                    'title'    => __( 'Order ID', 'woocommerce' ),
                    'desc'     => '',
                    'id'       => 'wcop_sync_order_id',
                    'type'     => 'text',
                    'default'  => '',
                    'class'    => 'wcop_sync_order_id'
                );
                $settings[] = array( 'type' => 'sectionend', 'id' => 'wcop-op-test-order' );
            }
            $settings[] = array( 'type' => 'title', 'title' => $valid?__( 'Sync Settings', 'woocommerce' ):'', 'desc' => $valid?'':'<div id="notice" class="error fade"><p>API connect failed. Confirm APP ID and API Key is correct.</p></div>', 'id' => 'wcop-order-sync' );
            if( $valid ) {
                $settings[] = array(
                    'title'    => __( 'Choose a date to start the sync from:', 'woocommerce' ),
                    'desc'     => 'Enter a date or leave empty to synchronize all orders from your woocommerce history.',
                    'id'       => 'wcop_sync_order_date',
                    'type'     => 'text',
                    'default'  => isset( $this->options['sync_order_date'] ) ? esc_attr( $this->options['sync_order_date']) : '',
                    'class'    => 'wcop_datepicker'
                );
                $settings[] = array(
                    'title'    => __( 'Order Status Filter', 'woocommerce' ),
                    'desc'     => 'Select the order status that you want to have synced.	If left blank the default is ‘Processing’ and ‘Completed’',
                    'id'       => 'wcop_sync_order_statuses',
                    'type'     => 'multiselect',
                    'class'    => 'wcop_multiselect',
                    'options'  => wc_get_order_statuses()
                );
                $settings[] = array(
                    'title'    => __( 'Turn on the Sync', 'woocommerce' ),
                    'desc'     => 'New Orders: When the Sync is turned ‘On’, all new Orders will process from Woocommerce to Ontraport and log the Ontraport Order ID into the Notes in the Woocommerce Order.<br><br>Historical Transactions: <a href="#" class="wcop_sync_history_btn button-primary">Click here to Start Sync</a>',//When the status is set to ‘On’ the sync script will work through all your orders from the start date above and process them in batches until they are up to date. The sync runs every 5 minutes and will process up to 100 transactions in each batch.
                    'id'       => 'wcop_sync_order',
                    'type'     => 'select',
                    'default'  => isset( $this->options['sync_order'] ) ? esc_attr( $this->options['sync_order']) : '0',
                    'options'  => array(
                        '0' => 'No',
                        '1' => 'Yes',
                    )
                );
            }
            $settings[] = array( 'type' => 'sectionend', 'id' => 'wcop-order-sync' );
            if( $valid ) {
                if( isset( $this->options['sync_order'] ) && $this->options['sync_order'] == "1" ){
                    $settings[] = array( 'type' => 'title', 'title' => __( 'Sync Log', 'woocommerce' ), 'desc' => '<div class="wcop-sync-log">'.get_option("wcop_sync_log").'</div>', 'id' => 'wcop-op-sync-log' );
                    $settings[] = array( 'type' => 'sectionend', 'id' => 'wcop-op-sync-log' );
                }

                $settings[] = array( 'type' => 'title', 'title' => __( 'Reset the Integration', 'woocommerce' ), 'desc' => 'WARNING: By resetting the plugin data you will delete the log files that sync the Woocommerce Order IDs from Ontraport Transactions together that help prevent duplication. Also all Product mapping from Ontraport Product IDs into Woocommerce Products will be lost and need to be re-mapped.<br><br><a href="admin.php?page=wc-settings&tab=wcop&clean_database" class="button button-primary" onclick="return confirm(\'WARNING: By resetting the plugin data you will delete the log files that sync the Woocommerce Order IDs from Ontraport Transactions together that help prevent duplication. Also all Product mapping from Ontraport Product IDs into Woocommerce Products will be lost and need to be re-mapped.\')">Reset</a>', 'id' => 'wcop-op-clean-orders' );
                $settings[] = array( 'type' => 'sectionend', 'id' => 'wcop-op-clean-orders' );
            }
        }
        return apply_filters( 'wc_settings_tab_wcop', $settings );
    }
    function settings_tab( $tabs ) {
        $tabs['wcop'] = esc_html__( 'Ontraport', 'woocommerce' );
        return $tabs;
    }
    function settings_page() {
        woocommerce_admin_fields( $this->get_settings() );
    }
    function save_settings($array) {
        woocommerce_update_options( $this->get_settings() );
    }
    function admin_scripts(){
        wp_enqueue_style( 'select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css' );
        wp_enqueue_script( 'select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js' );
        wp_enqueue_script( 'jquery-ui-datepicker' );
    }
    function footer(){
        ?>
        <script>
            jQuery(document).ready(function(){
                jQuery( ".wcop_datepicker" ).datepicker({dateFormat: "dd/mm/yy"});
                jQuery( ".wcop_multiselect" ).select2();
                if(jQuery( "#wcop_sync_order_id" ).length > 0) {
                    jQuery( "#wcop_sync_order_id" ).after('<span class="description"><button type="button" class="wcop_test_sync_btn button-primary">Sync</button><button type="button" class="wcop_sync_btn button-secondary">Sync 10 most recent Order IDs</button></span><span class="wcop-loading">Processing...</span><span class="wcop-loading wcop-sync-result"><span class="wcop-response"></span><span class="wcop-close"><span class="dashicons dashicons-no"></span></span></span>');
                    jQuery( ".wcop_test_sync_btn" ).click(function(){
                        jQuery(".wcop-sync-result").hide();
                        if( jQuery( "#wcop_sync_order_id" ).val() != '' ) {
                            jQuery(".wcop-loading:not(.wcop-sync-result)").show();
                            jQuery.post('<?php echo admin_url("admin-ajax.php")?>', {action: 'wcop_sync_test_order', order_id:jQuery( "#wcop_sync_order_id" ).val() }, function( response ){
                                jQuery(".wcop-loading").hide();
                                jQuery(".wcop-sync-result .wcop-response").html(response);
                                jQuery(".wcop-sync-result").show();
                            });
                        }
                        else{
                            alert("Enter Order ID.");
                        }
                    });
                    jQuery( ".wcop_sync_btn" ).click(function(){
                        jQuery(".wcop-loading").show();
                        jQuery(".wcop-sync-result").hide();
                        var i = 0;
                        wcop_sync_first_orders(i);
                    });
                    jQuery( ".wcop_sync_history_btn" ).click(function(e){
                        e.preventDefault();
                        jQuery(".wcop-sync-log").show();
                        jQuery(".wcop-sync-log").html('<p>Starting Sync....</p>');
                        wcop_sync_history();
                    });
                    jQuery( ".wcop-close" ).click(function(){
                        jQuery(this).parent().hide();
                    });
                }
            });
            function wcop_sync_first_orders(i){
                if( i > 10 ) {
                    jQuery(".wcop-loading").hide();
                    jQuery(".wcop-sync-result").show();
                }
                else{
                    jQuery.post('<?php echo admin_url("admin-ajax.php")?>', {action: 'wcop_sync_test_order', first_orders }, function( response ){
                        jQuery(".wcop-sync-result .wcop-response").append(response);
                        if( response ){
                            i++;
                        }
                        else{
                            i=11;
                        }
                        wcop_sync_first_orders(i);
                    });
                }
            }
            function wcop_sync_history(){
                jQuery.post('<?php echo admin_url("admin-ajax.php")?>', {action: 'wcop_sync_the_orders' }, function( response ){
                    if( response ) {
                        jQuery(".wcop-sync-log").append(response);
                        wcop_sync_history();
                    }
                    else{
                        jQuery(".wcop-sync-log").append('<p>All orders are synchronised now!</p>');
                    }
                });
            }
        </script>
        <style>
            span.description{ display:block;}
            button.wcop_test_sync_btn.button-primary,button.wcop_sync_btn.button-secondary {
                margin-left: 5px;
                min-width: 75px;
                margin-top: 2px;
            }
            span.wcop-loading {
                position: absolute;
                width: 100%;
                height: 100%;
                left: 0;
                background-color: rgba(0,0,0,0.75);
                top: 0;
                text-align: center;
                color: #fff;
                padding-top: 21px;
                box-sizing: border-box;
                display:none;
            }
            span.wcop-sync-result {
                top: 100%;
            }
            span.wcop-close {
                position: absolute;
                right: 0;
                top: 0;
                width: 60px;
                height: 60px;
                display: block;
                text-align: center;
                cursor: pointer;
                box-sizing: border-box;
            }
            span.wcop-close span.dashicons{
                font-size: 28px;
                line-height: 60px;
            }
            .wcop-sync-log {
                max-height: 500px;
                overflow: auto;
                border: solid 1px;
                padding: 10px;
            }
        </style>
        <?php
    }
    function product_fields($loop, $variation_data, $variation){
        woocommerce_wp_text_input(
            array(
                'id'          => 'op16_Product_id[' . $variation->ID . ']',
                'label'       => __( 'op16_Product_id', 'woocommerce' ),
                'placeholder' => 'OP Product ID',
                'desc_tip'    => 'true',
                'description' => __( 'Linked product on ontraport.', 'woocommerce' ),
                'value'       => get_post_meta( $variation->ID, 'op16_Product_id', true )
            )
        );
    }
    function save_product_fields( $post_id ){
        if(isset($_POST['op16_Product_id'][ $post_id ])){
            $field_value = $_POST['op16_Product_id'][ $post_id ];
            if( empty( $field_value ) ) {
                //$field_value = $this->get_product_id( wc_get_product( $post_id ) );
            }
            update_post_meta( $post_id, 'op16_Product_id', esc_attr( $field_value ) );
        }
    }
    function product_fields_simple(){
        global $post;
        woocommerce_wp_text_input(
            array(
                'id'          => 'simple_op16_Product_id',
                'label'       => __( 'OP Product ID', 'woocommerce' ),
                'placeholder' => 'OP Product ID',
                'desc_tip'    => 'true',
                'description' => __( 'Linked product on ontraport.', 'woocommerce' ),
                'value'       => get_post_meta( $post->ID, '_op16_Product_id', true )
            )
        );
    }
    function save_product_fields_simple( $post_id ) {
        if(isset( $_POST['simple_op16_Product_id'] )){
            update_post_meta( $post_id, '_op16_Product_id', esc_attr( $_POST['simple_op16_Product_id'] ) );
        }
    }
    function admin_notices() {
        echo '<div id="notice" class="updated fade"><p>WooCommerce to Ontraport Sync is not configured yet. <a href="admin.php?page=wc-settings&tab=wcop">Please do it now</a>.</p></div>';
    }
    function init(){
        if ( !class_exists( 'woocommerce' ) ) {
            add_action( 'admin_notices', function(){
                echo '<div id="notice" class="updated fade"><p>WooCommerce to Ontraport Sync requires WooCommerce.</p></div>';
            } );
            return;
        }
        $this->get_options();
        if( !$this->is_configured() ) {
            add_action( 'admin_notices', array( $this, 'admin_notices' ) );
        }
        if( isset( $_GET[ "wcop" ] ) ){
            $this->ontraport_tracking_code( $_GET[ "wcop" ] );
            die;
        }
        add_filter( 'woocommerce_settings_tabs_array', array( $this, 'settings_tab' ), 50 );
        add_action( 'woocommerce_settings_tabs_wcop', array( $this, 'settings_page' ));
        add_action( 'woocommerce_update_options_wcop', array( $this, 'save_settings' ));
        //simple product
        add_action( 'woocommerce_product_options_general_product_data', array( $this, 'product_fields_simple' ));
        add_action( 'woocommerce_process_product_meta', array( $this, 'save_product_fields_simple' ));
        add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'product_fields' ), 10, 3 );
        add_action( 'woocommerce_save_product_variation', array( $this, 'save_product_fields' ), 10, 2 );
        add_action( 'wp_ajax_wcop_sync_test_order', array( $this, 'sync_test_order' ) );
        if( isset( $_GET[ "tab" ] ) && $_GET[ "tab" ] == 'wcop' ) {
            remove_action( 'admin_notices', array( $this, 'admin_notices' ) );
        }
        if( isset( $_GET[ "delete_order_sync" ] ) ) {
            delete_post_meta_by_key( 'op46_Transaction_id' );
            add_action( 'admin_notices', function(){
                echo '<div id="notice" class="updated fade"><p>All orders are set to resynchronized successfully.</a>.</p></div>';
            });
        }
        if( isset( $_GET[ "clean_database" ] ) ) {
            delete_post_meta_by_key( 'op46_Transaction_id' );
            delete_post_meta_by_key( 'op46_Transaction_error' );
            delete_post_meta_by_key( 'op16_Product_id' );
            global $wpdb;
            $comments = $wpdb->get_results("select * from ".$wpdb->prefix."comments where comment_date >= '2019-06-08 00:00:00' and comment_content like '%ontraport%'");
            foreach( $comments as $comment ) {
                wp_delete_comment( $comment->comment_ID, true );
            }
            delete_option( "wcop_app_id" );
            delete_option( "wcop_api_key" );
            delete_option( "wcop_program" );
            delete_option( "wcop_sync_order" );
            delete_option( "wcop_sync_order_date" );
            delete_option( "wcop_sync_order_statuses" );
            delete_option( "wcop_sync_tax" );
            delete_option( "wcop_op_tax" );
            delete_option( "wcop_last_sync" );
            delete_option( "wcop_sync_log" );
            delete_option( "wcop_product_name_prefix" );
            delete_option( "wcop_op_tag" );
            wp_redirect( "admin.php?page=wc-settings&tab=wcop&wcop_clean_done" );
        }
        if( isset( $_GET[ "wcop_clean_done" ] ) ){
            add_action( 'admin_notices', function(){
                echo '<div id="notice" class="updated fade"><p>Database has been cleaned successfully.</a>.</p></div>';
            });
        }
        if( $this->options['sync_order'] == '1' ) {//&& isset($_GET[ "sync_order" ])
            if( !empty($this->options['program']) ) {
                add_action( 'woocommerce_thankyou', array( $this, 'ontraport_tracking_code' ) );
            }
            else{
                add_action( 'woocommerce_thankyou', array( $this, 'sync_order' ) );
            }
            add_action( 'wp_ajax_wcop_sync_the_orders', array( $this, 'sync_the_orders_ajax' ));
            /*$last_sync = absint(get_option( "wcop_last_sync" ));
            $sync_running = get_option( "wcop_sync_running" );
            $diff = time()-$last_sync;
            if( $diff > 60 && empty($sync_running) && $this->is_configured() ) {
                $valid = $this->ontraport->is_valid();
                if( $valid ) {
                    update_option( "wcop_sync_running", "yes" );
                    update_option( "wcop_last_sync", time() );
                    $sync_log = get_option( "wcop_sync_log" );
                    $sync_log = $sync_log.$this->sync_the_orders();
                    update_option( "wcop_sync_log", $sync_log );
                    delete_option( "wcop_sync_running" );
                }
            }
            add_action( 'wp_footer', function(){
                //delete_option( "wcop_sync_running" );
            } );*/
        }
    }
    function sync_test_order(){
        if( current_user_can( "manage_options" ) ){
            if( isset( $_POST[ "order_id" ] ) ) {
                $order = get_post( absint( $_POST[ "order_id" ] ) );
                if( !is_wp_error( $order ) && $order->post_type == 'shop_order' ) {
                    $meta = get_post_meta( $order->ID, 'op46_Transaction_id', true );
                    if( $meta == '' ){
                        echo $this->sync_order( $order->ID );
                    }
                    else{
                        echo "Order #".$order->ID." already syncronized. (Invoice ID: ".$meta.")";
                    }
                }
                else{
                    echo "This is not an order.";
                }
            }
            else if( isset( $_POST[ "first_orders" ] ) ) {
                echo $this->sync_the_orders();
            }
        }
        wp_die();
    }
    function sync_the_orders_ajax(){
        if( current_user_can( "manage_options" ) ){
            echo $this->sync_the_orders();
        }
        wp_die();
    }
    function sync_the_orders(){
        $rtn = '';
        if( current_user_can( "manage_options" ) ){
            $args = array(
                'post_type' => 'shop_order',
                'posts_per_page' => '1',
                'meta_query' => array(
                    array(
                        'key' => 'op46_Transaction_id',
                        'compare' => 'NOT EXISTS'
                    )
                )
            );
            if( !empty( $this->options['sync_order_date'] ) ) {
                $args[ "date_query" ] = array(
                    array(
                        'after' => date("Y-m-d", strtotime(str_replace( '/', '-', $this->options['sync_order_date']))),
                    )
                );
            }
            if( is_array( $this->options['sync_order_statuses'] ) && count( $this->options['sync_order_statuses'] ) > 0 ) {
                $args[ 'post_status' ] = $this->options['sync_order_statuses'];
            }
            else{
                $args[ 'post_status' ] = 'any';
            }
            $orders = get_posts($args);
            if( count( $orders ) > 0 ) {
                foreach( $orders as $order ) {
                    $rtn .= $this->sync_order( $order->ID )."<br>";
                }
            }
        }
        return $rtn;
    }
    function sync_order( $order_id ){
        //return "Testing order: ".$order_id;
        $meta = get_post_meta( $order_id, 'op46_Transaction_id', true );
        if( $meta != '' ){
            return;
        }
        update_post_meta( $order_id, 'op46_Transaction_id', "1" );
        $order = new WC_Order($order_id);
        $email = $order->get_billing_email();
        //$email = "waqar@itmooti.com.au";
        if( !empty( $email ) ) {
            $contact = $this->ontraport->findContactByEmail( $email );
            if( !is_object( $contact ) ) {
                $contact = array(
                    "firstname" => $order->get_shipping_first_name()?$order->get_shipping_first_name():$order->get_billing_first_name(),
                    "lastname" => $order->get_shipping_last_name()?$order->get_shipping_last_name():$order->get_billing_last_name(),
                    "email" => $email,
                    "address" => $order->get_shipping_address_1()?$order->get_shipping_address_1():$order->get_billing_address_1(),
                    "address2" => $order->get_shipping_address_2()?$order->get_shipping_address_2():$order->get_billing_address_2(),
                    "city" => $order->get_shipping_city()?$order->get_shipping_city():$order->get_billing_city(),
                    "state" => $order->get_shipping_state()?$order->get_shipping_state():$order->get_billing_state(),
                    "zip" => $order->get_shipping_postcode()?$order->get_shipping_postcode():$order->get_billing_postcode(),
                    "sms_number" => $order->get_billing_phone(),
                    "company" => $order->get_shipping_company()?$order->get_shipping_company():$order->get_billing_company(),
                    "country" => $order->get_shipping_country()?$order->get_shipping_country():$order->get_billing_country(),
                );
                $contact = $this->ontraport->saveUserData( 0, $contact );
            }
            $products = array();
            $order_items = $order->get_items( apply_filters( 'woocommerce_purchase_order_item_types', 'line_item' ) );
            $shipping = $order->get_shipping_total();
            $op_taxes = array();
            $wcop_sync_tax = isset( $this->options['sync_tax'] ) ? esc_attr( $this->options['sync_tax']) : '0';
            if( $wcop_sync_tax == "1" ) {
                $taxes = $order->get_tax_totals();
                if(is_array( $taxes )) {
                    foreach( $taxes as $tax ) {
                        $op_taxes[] = array(
                            "id"			=> isset( $this->options["op_tax"][$tax->rate_id] )?$this->options["op_tax"][$tax->rate_id]:'0',
                            "name"			=> $tax->label,
                            "taxTotal"			=> $tax->amount,
                        );
                    }
                }
            }
            foreach ( $order_items as $item_id => $item ) {
                $product = $item->get_product();
                $tax = 0;
                if( $wcop_sync_tax == "1" ) {
                    $price = $order->get_item_total( $item, false, true );
                    $tax = $order->get_item_total( $item, true, true )-$price;
                }
                else{
                    $price = $order->get_item_total( $item, true, true );
                }
                $products[] = array(
                    "quantity"  => $item->get_quantity(),
                    "id"        => $this->get_product_id( $product, $item->get_name() ),
                    "price"     => array(
                        array(
                            "price" 	=> $price,
                        )
                    ),
                    "shipping" 	=> $shipping>0,
                    "taxable" 	=> $tax>0,
                    "tax" 	=> $tax>0
                );
            }
            $requestParams = array(
                "contact_id"       => $contact->id,
                "trans_date" 	   => strtotime( $order->get_date_created() ) * 1000,
                "invoice_template" => -1,
                "chargeNow"        => "chargeLog",
                "offer"            => array(
                    "products"     	=> $products,
                    "shipping"		=> array(
                        array(
                            "name"			=> "Shipping",
                            "price"			=> $shipping,
                        )
                    ),
                    "taxes"		=> $op_taxes,
                    "hasShipping" 	=> $shipping>0,
                    "hasTaxes" 	=> count($op_taxes)>0
                )
            );
            //print_r($requestParams); die;
            $object = $this->ontraport->manual_transaction( $requestParams );
            if( isset( $object->invoice_id ) ) {
                update_post_meta( $order->get_id(), 'op46_Transaction_id', $object->invoice_id );
                $response = 'Order #'.$order->get_id().' Synced to Ontraport - <a href="https://app.ontraport.com/#!/contact/edit&id='.$contact->id.'" target="_blank">Contact ID '.$contact->id.'</a> Invoice number '.$object->invoice_id;
                $order->add_order_note( $response );
                return $response;
            }
            else{
                //delete_post_meta( $order->get_id(), 'op46_Transaction_id' );
                update_post_meta( $order->get_id(), 'op46_Transaction_error', json_decode($object) );
            }
        }
    }
    function get_product_id( $product, $product_name = '' ){

        $op_product_id = get_post_meta( $product->get_id(), 'op16_Product_id', true );
        if( empty( $op_product_id ) ) {
            $op_product_id = get_post_meta( $product->get_id(), '_op16_Product_id', true );
        }
        if( empty( $op_product_id ) ) {
            $product_name = empty($product_name)?$product->get_title():$product_name;
            $op_product = array(
                "name" => (isset( $this->options['product_name_prefix'] ) ? esc_attr( $this->options['product_name_prefix']) : '').$product_name,
                "price" => 0
            );
            $ontraport_product = $this->ontraport->saveObject( "product", 0, $op_product );
            $op_product_id = $ontraport_product->id;
            update_post_meta( $product->get_id(), 'op16_Product_id', $op_product_id );
            update_post_meta( $product->get_id(), '_op16_Product_id', $op_product_id );
        }
        return $op_product_id;
    }
    function ontraport_tracking_code($order_id) {
        global $woocommerce;
        $order = new WC_Order( $order_id );
        $ontraportcode = '<!-- Begin Ontraport Pixel Tracking -->';
        $ontraportcode .= '<img src="https://'.$this->options['program'].'.ontraport.com/p?order_id=';
        $ontraportcode .= str_replace("#","",urlencode( $order->get_order_number() ));
        $found = false;
        if(is_user_logged_in()){
            $user = wp_get_current_user();
            $email = $user->user_email;
            if( $order->get_billing_email() != $email ) {
                $ontraportcode .= '&email=';
                $ontraportcode .= urlencode($email);
                $found = true;
            }
        }
        if( !$found ) {
            $ontraportcode .= '&email=';
            $ontraportcode .= urlencode($order->get_billing_email());
            $ontraportcode .= '&First_Name=';
            $ontraportcode .= urlencode($order->get_billing_first_name());
            $ontraportcode .= '&Last_Name=';
            $ontraportcode .= urlencode($order->get_billing_last_name());
        }
        if ( sizeof( $order->get_items() )>0 ) {
            $i = 1;
            foreach ( $order->get_items() as $item_id => $item ) {
                $product = $item->get_product();
                $ontraportcode .= '&item_id_'.$i.'=';
                $ontraportcode .= $this->get_product_id( $product, $item->get_name() );
                $ontraportcode .= '&item_external_id_'.$i.'=';
                $ontraportcode .= urlencode($item['name']);
                $ontraportcode .= '&item_qty_'.$i.'=';
                $ontraportcode .= $item['qty'];
                $ontraportcode .= '&item_price_'.$i.'=';
                $ontraportcode .= $order->get_item_total($item);
                //$ontraportcode .= $item['line_total']/$item['qty'];
                $i++;
            }
        }
        $ontraportcode .= '&Address=';
        $ontraportcode .= urlencode($order->get_billing_address_1());
        $ontraportcode .= '&Address_2=';
        $ontraportcode .= urlencode($order->get_billing_address_2());
        $ontraportcode .= '&City=';
        $ontraportcode .= urlencode($order->get_billing_city());
        $ontraportcode .= '&State=';
        $ontraportcode .= urlencode($order->get_billing_state());
        $ontraportcode .= '&Zip_Code=';
        $ontraportcode .= urlencode($order->get_billing_postcode());
        $ontraportcode .= '&Country=';
        $ontraportcode .= urlencode($order->get_billing_country());
        $ontraportcode .= '&shipping_amt=';
        $ontraportcode .= urlencode($order->get_shipping_total());
        $ontraportcode .= '&tax=';
        $ontraportcode .= urlencode(wc_round_tax_total( $order->get_cart_tax() + $order->get_shipping_tax() ));
        $ontraportcode .= '&contact_cat='.urlencode('*/*'.$this->options["op_tag"].'*/*');
        $ontraportcode .='" WIDTH="1" HEIGHT="1"/>
		<!-- End Ontraport Pixel Tracking -->';
        echo $ontraportcode;
        update_post_meta( $order->get_id(), 'op46_Transaction_id', "1" );
        $response = 'Order #'.$order->get_id().' Synced to Ontraport using Partners Tracking Pixel';
        $order->add_order_note( $response );
    }
}
$wcop=new wcop();
