=== WooCommerce to Ontraport Sync ===
Contributors: ITMOOTI
Tags: ontraport, woocommerce
Requires at least: 4.0
Tested up to: 5.4
Stable tag: 1.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A plugin to synchronize WooCommerce sales to Ontraport Sales record with contacts and partners.

== Description ==

= WooCommerce to Ontraport Sync =

A plugin to synchronize WooCommerce sales to Ontraport Sales record with contacts and partners.


== Screenshots ==

= How to use plugin? =

Download & install the plugin into your site. After successful installation of the plugin go to WooCommerce Settings Page to configure the plugin's option.


 == Installation ==

== Frequently Asked Questions ==

== Changelog ==

=1.0 =

Standard release


== Upgrade Notice ==
This is an initial release.

